set number
set mouse=""

filetype plugin indent on
set history=1000                " Store a ton of history (default is 20)

" Persistent undo
set undofile                    " Persist undo tree across launches
set undolevels=1000             " Maximum number of changes that can be undone
set undoreload=10000            " Maximum number lines to save for undo on a buffer reload

" Tab stuff
set autoindent                  " Indent at the same level of the previous line
set shiftwidth=4                " Use indents of 4 spaces
set expandtab                   " Tabs are spaces, not tabs
set tabstop=4                   " An indentation every four columns
set softtabstop=4               " Let backspace delete indent

" Add some extra word separators
set iskeyword-=_

" Color scheme
colorscheme Revolution


" Specify a directory for plugins
" - For Neovim: stdpath('data') . '/plugged'
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.local/share/nvim/plugged')

Plug 'dense-analysis/ale'

" Initialize plugin system
call plug#end()

" Autodetect some filetypes and turn on spellcheck 
" and 80-character lines when needed
augroup FiletypeGroup
    autocmd!
    " .ts is a Typescript file
    au BufNewFile,BufRead *.ts set filetype=typescript
    au BufNewFile,BufRead *.lalrpop set filetype=rust
    au BufRead,BufNewFile *.md setlocal textwidth=80 | set spell
    au BufRead,BufNewFile *.txt setlocal textwidth=80 | set spell
    au BufRead,BufNewFile *.tex set spell

augroup END

